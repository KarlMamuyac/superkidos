<h1 align="center">
    SuperKidos Game
</h1>

<h3 align="center">A match-3, combat game for the IP "SuperKidos"</h3>

<div align="center">
 
Alpha Screenshots
<details><summary>Click to expand</summary>
<img src="https://gitlab.com/KarlMamuyac/superkidos/-/raw/development/screenshots/Image%20Sequence_001_0000.jpg?ref_type=heads"/><img src="https://gitlab.com/KarlMamuyac/superkidos/-/raw/development/screenshots/Image%20Sequence_002_0000.jpg?ref_type=heads"/><img src="https://gitlab.com/KarlMamuyac/superkidos/-/raw/development/screenshots/Image%20Sequence_003_0000.jpg?ref_type=heads"/><img src="https://gitlab.com/KarlMamuyac/superkidos/-/raw/development/screenshots/Image%20Sequence_004_0000.jpg?ref_type=heads"/><img src="https://gitlab.com/KarlMamuyac/superkidos/-/raw/development/screenshots/Image%20Sequence_006_0000.jpg?ref_type=heads"/><img src="https://gitlab.com/KarlMamuyac/superkidos/-/raw/development/screenshots/Image%20Sequence_007_0000.jpg?ref_type=heads"/>
</details>
 </div>

<div align="center">
 
Gameplay
<details><summary>Click to expand</summary>
<img alt="Gameplay video" src="https://gitlab.com/KarlMamuyac/superkidos/-/raw/development/screenshots/Movie_001.mp4?ref_type=heads" />
</details>
 </div>