﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationHandler : MonoBehaviour
{
    [Header("Player Animators")]
    public Animator girlAnim;
    public Animator boyAnim;
    public Animator dogAnim;

    [Header("Player FX")]
    public Animator girlFX;
    public Animator boyFX;
    public Animator dogFX;
    public Animator partyFX;

    bool isAttacking;
    int atkCombo;
    bool isIcewall;

    void Start(){
        PlayIdle();
        isAttacking = false;
        atkCombo = 0;
        isIcewall = false;
    }

    public void PlayIdle(){
        PlayGirlIdle();
        PlayBoyIdle();
        PlayDogIdle();
    }

    public void PlayGirlIdle(){
        girlAnim.Play("Base.idle", 0, 0f);
        girlFX.Play("Base.idle", 0, 0f);
    }

    public void PlayBoyIdle(){
        boyAnim.Play("Base.idle", 0, 0f);
        if(isIcewall){
            boyFX.Play("Base.icewall_idle", 0, 0f);
        }
        else{
            boyFX.Play("Base.idle", 0, 0f);
        }
    }

    public void PlayDogIdle(){
        dogAnim.Play("Base.idle", 0, 0f);
        dogFX.Play("Base.idle", 0, 0f);
    }

    public void PlayPartyIdle(){
        partyFX.Play("Base.idle", 0, 0f);
    }

    public void PlayBreak(){
        isIcewall = false;
        boyFX.Play("Base.hit", 0, 0f);
    }

    public void PlayAttack(){
        atkCombo++;
        if(atkCombo < 3){
            if(!isAttacking){
                girlAnim.Play("Base.attack1", 0, 0f);
                girlFX.Play("Base.attack_1", 0, 0f);
                isAttacking = true;
                StartCoroutine(ReturnGirlToIdle(girlAnim.GetCurrentAnimatorStateInfo(0).length));
            }
        }
        else if(atkCombo >= 4){
            int val = Random.Range(0, 3);
            if(!isAttacking){
                if(val == 1){
                    girlAnim.Play("Base.attack2", 0, 0f);
                    girlFX.Play("Base.attack_2", 0, 0f);
                    isAttacking = true;
                    StartCoroutine(ReturnGirlToIdle(girlAnim.GetCurrentAnimatorStateInfo(0).length));
                }
                if(val == 2){
                    girlAnim.Play("Base.attack3", 0, 0f);
                    girlFX.Play("Base.attack_3", 0, 0f);
                    isAttacking = true;
                    StartCoroutine(ReturnGirlToIdle(girlAnim.GetCurrentAnimatorStateInfo(0).length));
                }
            }
        }
        if(atkCombo == 11){
            atkCombo = 0;
        }
    }

    public void PlayHeal(){
        dogAnim.Play("Base.heal", 0, 0f);
        dogFX.Play("Base.heal", 0, 0f);
        StartCoroutine(ReturnDogToIdle(dogAnim.GetCurrentAnimatorStateInfo(0).length));
    }

    public void PlayDefend(){

        if(!isIcewall){
            isIcewall = true;
            boyFX.Play("Base.defend", 0, 0f);
        }
        boyAnim.Play("Base.defend", 0, 0f);
        StartCoroutine(ReturnBoyToIdle(boyAnim.GetCurrentAnimatorStateInfo(0).length));
    }

    IEnumerator ReturnGirlToIdle(float delay){
        yield return new WaitForSeconds(delay);
        isAttacking = false;
        PlayGirlIdle();
    }

    IEnumerator ReturnBoyToIdle(float delay){
        yield return new WaitForSeconds(delay);
        PlayBoyIdle();
    }

    IEnumerator ReturnDogToIdle(float delay){
        yield return new WaitForSeconds(delay);
        PlayDogIdle();
        partyFX.Play("Base.heal", 0, 0f);
        StartCoroutine(ReturnPartyToIdle(partyFX.GetCurrentAnimatorStateInfo(0).length));
    }

    IEnumerator ReturnPartyToIdle(float delay){
        yield return new WaitForSeconds(delay);
        PlayPartyIdle();
    }
}
