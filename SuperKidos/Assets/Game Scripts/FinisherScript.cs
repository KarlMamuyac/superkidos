﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;

public class FinisherScript : MonoBehaviour
{
    public CombatScript combatTracker;
    public HealthScript playerHealth;
    public HealthScript enemyHealth;

    public GameObject menuPanel;
    public GameObject finisherPanel;
    public GameObject winPanel;
    public GameObject losePanel;
    public GameObject coverPanel;
    public Animator topOverlay;

    bool won;
<<<<<<< HEAD
    public bool bossFight;
    
    public GameObject finisherOverlay;
    private SkeletonGraphic finisherSke;
    private Animator finisherAnim;
    public GameObject playerSide;
    public GameObject enemySide;

    public SkeletonGraphic playerFXSke;
    public SkeletonGraphic monsterFXSke;

    public AudioSource bgAudio;
    public AudioClip[] clips;
=======

    public EnemyAnimationHandler enemyAnim;
>>>>>>> development

    void Start(){
        PlayAudio(0);

        finisherSke = finisherOverlay.GetComponentInChildren<SkeletonGraphic>();
        finisherAnim = finisherOverlay.GetComponentInChildren<Animator>();
        finisherOverlay.SetActive(false);

        playerSide.SetActive(true);
        enemySide.SetActive(true);

        finisherPanel.SetActive(false);
        winPanel.SetActive(false);
        losePanel.SetActive(false);
        menuPanel.SetActive(false);
        won = false;

    }

    void Update(){
        if(enemyHealth.HealthLessThan(0) || enemyHealth.HealthEquals(0)){
            if(bossFight){
                if(!won){
                    menuPanel.SetActive(true);
                    finisherPanel.SetActive(true);
                    coverPanel.SetActive(true);
                }
            }
            if(!bossFight){
                if(!won){
                    NonBossFinish();
                }
            }
        }

        if(playerHealth.HealthLessThan(0) || playerHealth.HealthEquals(0)){
            Lose();
            coverPanel.SetActive(true);
        }
    }

<<<<<<< HEAD
    void PlayAudio(int clipNum){
        bgAudio.clip = clips[clipNum];
        bgAudio.Play();
    }

    public void NonBossFinish(){
        won = true;
        finisherPanel.SetActive(false);
        StartCoroutine(NonBossSequence());
    }

    IEnumerator NonBossSequence(){
        monsterFXSke.Skeleton.SetSkin("Explosion");
        yield return new WaitForSpineAnimationComplete(monsterFXSke.AnimationState.SetAnimation(0, "Explosion_with_BOOM", false));
        enemySide.SetActive(false);
        menuPanel.SetActive(true);
        playerSide.SetActive(false);
        winPanel.SetActive(true);
    }

    public void CorrectFinisher(){
        won = true;
        coverPanel.SetActive(true);
        finisherPanel.SetActive(false);
        menuPanel.SetActive(false);
        StartCoroutine(FinisherSequence());
    }

    IEnumerator FinisherSequence(){
        finisherOverlay.SetActive(true);
        playerSide.SetActive(false);
        finisherAnim.Play("Base.fly_in", 0, 0f);
        yield return new WaitForSpineAnimationComplete(finisherSke.AnimationState.SetAnimation(0, "Fly_In", false));
        yield return new WaitForSpineAnimationComplete(finisherSke.AnimationState.SetAnimation(0, "FINISHER", false));
        monsterFXSke.AnimationState.SetAnimation(0, "Explosion_with_BOOM", false);
        finisherSke.AnimationState.SetAnimation(0, "Idle", false);
        yield return new WaitForSeconds(1f);
        enemySide.SetActive(false);
        menuPanel.SetActive(true);
=======
    IEnumerator EnemyDies(){
        won = true;
        finisherPanel.SetActive(false);
        enemyAnim.PlayDead();
        yield return new WaitForSeconds(1.25f);
>>>>>>> development
        winPanel.SetActive(true);
    }

    public void CorrectFinisher(){
        StartCoroutine(EnemyDies());
        topOverlay.Play("Base.win", 0, 0f);
    }

    public void WrongFinisher(){
        menuPanel.SetActive(false);
        finisherPanel.SetActive(false);
        coverPanel.SetActive(false);
        enemyHealth.AddHealth(75);
    }

    void Lose(){
        menuPanel.SetActive(true);
        finisherPanel.SetActive(false);
        winPanel.SetActive(false);
        losePanel.SetActive(true);
    }
}
