﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopScript : MonoBehaviour
{
    private int currentEnergy;
    public Text energyText;

    void Start()
    {
        currentEnergy = PlayerPrefs.GetInt("Energy", 5);
    }

    void Update()
    {
        energyText.text = (currentEnergy).ToString();
    }

    public void BuyEnergy(){
        currentEnergy++;
        PlayerPrefs.SetInt("Energy", currentEnergy);
    }
}
