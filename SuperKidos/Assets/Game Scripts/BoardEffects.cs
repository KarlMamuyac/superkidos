﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardEffects : MonoBehaviour
{
    public ArrayLayout fxLayout;

    [Header("Prefabs")]
    public GameObject nodePiece;
    public RectTransform gameBoard;
    public Sprite[] fx;
    int w = 7;
    int h = 7;
    EffectNode[,] fxBoard;

    public EffectHandler fxHandler;

    List<Point> cleared;

    void Start(){
        StartGame();
        cleared = new List<Point>();
    }

    void StartGame(){
        InitializeEffects();
        InstantiateEffects();
    }

    void InitializeEffects(){
        fxBoard = new EffectNode[w, h];

        for(int y = 0; y < h; y++){
            for(int x = 0; x < w; x++){
                fxBoard[x, y] = new EffectNode((fxLayout.rows[y].row[x]) ? -1 : FillEffect(), new Point(x, y));
            }
        }
    }

    void InstantiateEffects(){
        for(int x = 0; x < w; x++){
            for(int y = 0; y < h; y++){
                EffectNode block = GetBlockAtPoint(new Point(x, y));

                int val =  block.value;
                if(val <= 0 ) continue;
                GameObject p = Instantiate(nodePiece, gameBoard);
                BlockPiece piece = p.GetComponent<BlockPiece>();
                RectTransform rect = p.GetComponent<RectTransform>();
                rect.anchoredPosition = new Vector2(32 + (64 * x), -32 - (64 * y));
                piece.EffectsInitialize(val, new Point(x, y), fx[val - 1]);

                block.SetPiece(piece);
            }
        }
    }

    int FillEffect(){
        int val = 1;
        return val;
    }

    EffectNode GetBlockAtPoint(Point p){
        return fxBoard[p.x, p.y];
    }

    void Update() {
        foreach(Point pnt in fxHandler.GetCleared()){
            cleared.Add(pnt);
        }
        foreach(Point fx in cleared){
            EffectNode fxNode = GetBlockAtPoint(fx);
            BlockPiece fxBlock = fxNode.GetPiece();
            fxBlock.PlayPop();
            // Debug.Log("pop : " + fxBlock.GetIndex());
            cleared.Remove(fx);
        }

        // WIP reverse for loop for iteration conflict. will resolve 

        // for(int cnt = fxHandler.GetCleared().Count; cnt > 0; cnt--){
        //     cleared.Add(fxHandler.GetCleared());
        // }
        // for(int cnt2 = cleared.Count; cnt2 > 0; cnt2--){
        //     EffectNode fxNode = GetBlockAtPoint(fx);
        //     BlockPiece fxBlock = fxNode.GetPiece();;
        //     fxBlock.PlayPop();
        //     Debug.Log("pop : " + fxBlock.GetIndex());
        //     cleared.Remove(fx);
        // }
    }

}

[System.Serializable]
public class EffectNode{
    public int value;
    public Point index;
    BlockPiece piece;

    public EffectNode(int v, Point i){
        value = v;
        index = i;
    }

    public void SetPiece(BlockPiece p){

        piece = p;
        value = (piece == null) ? 0 : piece.value;

        if(piece == null){
            return;
        }

        piece.SetIndex(index);
    }

    public BlockPiece GetPiece(){
        return piece;
    }
}