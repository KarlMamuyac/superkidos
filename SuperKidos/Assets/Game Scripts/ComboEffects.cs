﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComboEffects : MonoBehaviour
{
    public Sprite[] comboSprites;
    public Image comboImage;
    Animator anim;

    void Start(){
        anim = comboImage.GetComponent<Animator>();
    }

    public void UpdateComboSprite(int value){
        comboImage.sprite = comboSprites[value-1];
        comboImage.SetNativeSize();

        switch(value){
            case 1:
                anim.Play("Base.good", 0, 0f);
                break;
            case 2:
                anim.Play("Base.great", 0, 0f);
                break;
            case 3:
                anim.Play("Base.cool", 0, 0f);
                break;
            case 4:
                anim.Play("Base.super", 0, 0f);
                break;
            case 5:
                anim.Play("Base.amazing", 0, 0f);
                break; 
            default:
                break;
        }
    }
}
