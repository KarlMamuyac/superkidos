﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBlock : MonoBehaviour
{
    public static MoveBlock instance;
    BoardScript game;
    BlockPiece moving;
    Point newIndex;
    Vector2 mouseStart;

    private void Awake(){
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        game = GetComponent<BoardScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if(moving != null){
            Vector2 dir = ((Vector2)Input.mousePosition - mouseStart);
            Vector2 nDir = dir.normalized;
            Vector2 aDir = new Vector2(Mathf.Abs(dir.x), Mathf.Abs(dir.y));

            newIndex = Point.ClonePoint(moving.index);

            Point add = Point.zero;

            //Move sensitivity : if the cursor
            if(dir.magnitude > 32){
                //Make add into one of the four directions
                //Visual moving
                if(aDir.x > aDir.y){
                    add = (new Point((nDir.x > 0) ? 1 : -1, 0));
                }
                else if(aDir.y > aDir.x){
                    add = (new Point(0, (nDir.y > 0) ? -1 : 1));
                }
            }
            newIndex.AddPoint(add);


            //Actual moving
            Vector2 pos = game.GetPointPosition(moving.index);
            if(!newIndex.Equals(moving.index)){
                // Offset piece to the direction of the move
                pos += Point.MultiplyPoint(new Point(add.x, -add.y), 16).PointToVector();
            }
            moving.MovePositionTo(pos);
        }
    }

    public void MovePiece(BlockPiece piece){
        if(moving != null){
            return;
        }
        moving = piece;
        mouseStart = Input.mousePosition;
    }

    public void DropPiece(){
        if(moving == null){
            return;
        }


        if(!newIndex.Equals(moving.index)){
            game.FlipPieces(moving.index, newIndex, true);
        }
        else{
            game.ResetPiece(moving);
        }

        game.ResetPiece(moving);
        moving = null;
    }
}
