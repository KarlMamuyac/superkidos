﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyScript : MonoBehaviour
{
    [Header("Monster Attributes")]
    public int monsterTurn;
    public int lightDamage;
<<<<<<< HEAD
    public int heavyDamage;
    public int monsterHP;
=======
    public int hp;
>>>>>>> development
    public HealthScript monsterHealth;
    public Text textCounter;

    void Start()
    {
<<<<<<< HEAD
        monsterHealth.SetMaxHealth(monsterHP);
=======
        monsterHealth.SetMaxHealth(hp);
        monsterAttack.SetMaxHealth(3);
        monsterAttack.SetHealth(monsterTurn);
>>>>>>> development
        IncrementText();
    }

    public int TurnCounter(){
        if(monsterTurn == 3){
            monsterTurn = 0;
            IncrementText();
            return 3;
        }
        IncrementText();
        return monsterTurn;
    }

    public void IncrementText(){
        textCounter.text = (3 - monsterTurn).ToString();
    }

    public void LoseHealth(int dmg){
        monsterHealth.SubHealth(dmg);
    }

    public void AddTurn(int addTurn){
        monsterTurn = monsterTurn + addTurn;
    }

    public void ReplaceBlocks(){

    }

    public int DealHeavyDamage(){
        return heavyDamage;
    }
}
