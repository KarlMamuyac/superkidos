﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadySetGo : MonoBehaviour
{
    public GameObject startPanel;
    public GameObject ready;
    public GameObject set;
    public GameObject go;
    public LevelHandler startFade;

    void Start(){
        startPanel.SetActive(true);
        ready.SetActive(false);
        set.SetActive(false);
        go.SetActive(false);
        StartCoroutine(InitiateGame());
    }

    IEnumerator InitiateGame(){
        yield return new WaitForSeconds(startFade.GetStartTime() + 1.5f);
        ready.SetActive(true);
        set.SetActive(false);
        go.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        ready.SetActive(false);
        set.SetActive(true);
        go.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        ready.SetActive(false);
        set.SetActive(false);
        go.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        startPanel.SetActive(false);
    }

}
