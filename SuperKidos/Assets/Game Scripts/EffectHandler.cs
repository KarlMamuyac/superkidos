﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectHandler : MonoBehaviour
{
    List<Point> cleared = new List<Point>();
    public void SetCleared(Point clearedBlock){
        cleared.Add(clearedBlock);
    }

    public List<Point> GetCleared(){
        List<Point> returnCleared = new List<Point>();
        foreach(Point pnt in cleared){
            returnCleared.Add(pnt);
        }
        cleared.Clear();
        return returnCleared;
    }
}
