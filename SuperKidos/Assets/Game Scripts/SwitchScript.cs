﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchScript : MonoBehaviour
{
    public CombatScript combatTracker;
    public HealthScript enemyHealth;

    void Update(){
        if(enemyHealth.HealthLessThan(0) || enemyHealth.HealthEquals(0)){
            combatTracker.SwitchEnemies();
        }
    }

}
