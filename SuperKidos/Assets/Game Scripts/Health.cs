﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Health {
    public int hp;

    public Health(int newhp){
        hp = newhp;
    }

    public void AddHealth(int add){
        hp = hp + add;
    }

    public void SubHealth(int sub){
        hp = hp - sub;
    }

    public bool HealthEquals(int hpref){
        return (hp == hpref);
    }
}
