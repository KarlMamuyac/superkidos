﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchAudioScript : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip[] clips;

    public void PlayAudio(int clipNum){
        audioSource.clip = clips[clipNum];
        audioSource.Play();
    }
}
