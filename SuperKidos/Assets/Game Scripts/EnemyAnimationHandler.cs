﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimationHandler : MonoBehaviour
{
    public Animator combatFX;
    public Animator topScreenFX;
    public Animator enemyAnim;

    bool isAttacking;

    void Start(){
        PlayIdle();
        isAttacking = false;
    }

    public void PlayIdle(){
        enemyAnim.Play("Base.idle", 0, 0f);
        topScreenFX.Play("Base.idle", 0, 0f);
    }

    public void PlayHit(){
        if(!isAttacking){
            enemyAnim.Play("Base.hit", 0, 0f);
            StartCoroutine(ReturnToIdle(enemyAnim.GetCurrentAnimatorStateInfo(0).length));
        }
    }

    public void PlayDead(){
        enemyAnim.Play("Base.dead", 0, 0f);
    }

    public void PlayAttack(){
        isAttacking = true;
        enemyAnim.Play("Base.attack", 0, 0f);
        StartCoroutine(StompToIdle(enemyAnim.GetCurrentAnimatorStateInfo(0).length));
    }

    IEnumerator ReturnToIdle(float delay){
        yield return new WaitForSeconds(delay - 0.25f);
        isAttacking = false;
        PlayIdle();
    }

    IEnumerator StompToIdle(float delay){
        yield return new WaitForSeconds(delay - 0.65f);
        combatFX.Play("Base.stomp", 0, 0f);
        topScreenFX.Play("Base.shake", 0, 0f);
        StartCoroutine(ReturnToIdle(enemyAnim.GetCurrentAnimatorStateInfo(0).length));
    }

}
