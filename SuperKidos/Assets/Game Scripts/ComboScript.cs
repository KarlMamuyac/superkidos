﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine;
using Spine.Unity;

public class ComboScript : MonoBehaviour
{

    public SkeletonGraphic girlSke;
    public SkeletonGraphic boySke;
    public SkeletonGraphic dogSke;

    private Skeleton girlSkin;
    private Skeleton boySkin;
    private Skeleton dogSkin;

    private int comboCounter;
    public AudioSource comboAudio;
    public AudioClip[] clips;
    public Sprite[] comboSprites;
    public Image comboImage;
    Animator anim;

    void Start(){
        girlSkin = girlSke.Skeleton;
        boySkin = boySke.Skeleton;
        dogSkin = dogSke.Skeleton;

        anim = comboImage.GetComponent<Animator>();
    }
    void PlayAudio(int clipNum){
        comboAudio.clip = clips[clipNum];
        comboAudio.Play();
    }
    public void AddToCombo(){
        comboCounter++;
        PlayAudio(0);
        if(comboCounter  > 3){
            UpdateComboSprite(1);
            PlayAudio(1);
            if(comboCounter > 6){
                UpdateComboSprite(2);
                PlayAudio(2);
                if(comboCounter > 9){
                    UpdateComboSprite(3);
                    PlayAudio(3);
                    girlSkin.SetSkin("Girl_Default_Aura");
                    boySkin.SetSkin("Boy_Default_Aura");
                    dogSkin.SetSkin("Dog_Default_Aura");
                    if(comboCounter > 15){
                        UpdateComboSprite(4);
                        PlayAudio(4);
                        if(comboCounter > 18){
                            UpdateComboSprite(5);
                            PlayAudio(4);
                            if(comboCounter > 25){
                                ResetCombo();
                            }
                        }
                    }
                }
            }
        }
    }

    public void ResetCombo(){
        comboCounter = 0;
        girlSkin.SetSkin("Girl_Default");
        boySkin.SetSkin("Boy_Default");
        dogSkin.SetSkin("Dog_Default");
    }
    public void UpdateComboSprite(int value){
        comboImage.sprite = comboSprites[value-1];
        comboImage.SetNativeSize();

        anim.Play("Base.flicker", 0, 0f);
    }
}
