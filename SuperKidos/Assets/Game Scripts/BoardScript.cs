﻿using System.Collections;
using System.Collections.Generic;
using System.Security;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BoardScript : MonoBehaviour
{  
    Scene scene;
    public TutorialScript tutorial;
    public ArrayLayout boardLayout;

    [Header("UI Elements")]
    public Sprite[] subPieces;
    public RectTransform gameBoard;
    public GameObject coverPanel;

    public CombatScript combat;

    [Header("Prefabs")]
    public GameObject nodePiece;

    Block[,] board;
    int w = 7;
    int h = 7;
    int[] fills;

    System.Random random;

    List<BlockPiece> update;
    List<BlockPiece> removed;
    List<FlippedPieces> flipped;

    Sprite[] pieces;
    bool falling;
    bool covered;

    [Header("FX")]
    public EffectHandler fxHandler;
<<<<<<< HEAD
    public ComboScript comboFx;
=======
    public ComboEffects comboFX;
    public MatchAudioScript audioScript;
    int comboCounter = 0;
>>>>>>> development

    /**
    function for block variations
    **/
    public void SetPieceNumber(int input){
        int pieceNum = input;

        if(pieceNum != PlayerPrefs.GetInt("Blocks")){
            PlayerPrefs.SetInt("Blocks", pieceNum);
            pieces = new Sprite[pieceNum];
            SceneManager.LoadScene(scene.name);
        }
        
    }

    void Start()
    {
        scene = SceneManager.GetActiveScene(); 
        PlayerPrefs.SetInt("Blocks", subPieces.Length);
        CreateGame();
    }

    void Update()
    {
        List<BlockPiece> finishedUpdating = new List<BlockPiece>();
        for(int i = 0; i < update.Count; i++){
            BlockPiece piece = update[i];
            if(!piece.UpdatePiece()){
                finishedUpdating.Add(piece);
            }
        }
        for(int i = 0; i < finishedUpdating.Count; i++){
            BlockPiece piece = finishedUpdating[i];
            FlippedPieces flip = GetFlipped(piece);
            BlockPiece flippedPiece = null;

            int x = (int) piece.index.x;
            fills[x] = Mathf.Clamp(fills[x] - 1, 0, w);

            List<Point> connected = isConnected(piece.index, true);

            bool wasFlipped = (flip != null);
            
            // if blocks were flipped
            if(wasFlipped){
                flippedPiece = flip.GetOtherPiece(piece);
                AddPoints(ref connected, isConnected(flippedPiece.index, true));
            }

            // no match flip
            if(connected.Count == 0){
                if(wasFlipped){
                    // flip back
                    FlipPieces(piece.index, flippedPiece.index, false);
                }
            }
            // else for making a match
            else{
                falling = true;
                // Remove connected pieces or matches
                foreach(Point pnt in connected){
                    Block block = GetBlockAtPoint(pnt);
                    BlockPiece blockPiece = block.GetPiece();
                    StartCoroutine(ClearBuffer());
                    if(blockPiece != null){
                        blockPiece.gameObject.SetActive(false);
                        removed.Add(blockPiece);

                        fxHandler.SetCleared(pnt);
                    }
                    block.SetPiece(null);

<<<<<<< HEAD
                    combat.Combat(blockPiece.GetBlockValue());
                    comboFx.AddToCombo();
=======
                    if(scene.name == "TutorialAttack"){
                        tutorial.TutorialCombat(blockPiece.GetBlockValue());
                    }
                    else{
                        combat.Combat(blockPiece.GetBlockValue());
                        AddToCombo();
                    }
>>>>>>> development
                }

                StartCoroutine(MoveBuffer());
                StartCoroutine(ClearBuffer());

            }

            flipped.Remove(flip);
            update.Remove(piece);
        }
    }

    public void AddToCombo(){
        comboCounter++;
        if(comboCounter  > 2){
            comboFX.UpdateComboSprite(1);
            audioScript.PlayAudio(0);

            if(comboCounter > 4){
                comboFX.UpdateComboSprite(2);
                audioScript.PlayAudio(1);

                if(comboCounter > 6){
                    comboFX.UpdateComboSprite(3);
                    audioScript.PlayAudio(2);

                    if(comboCounter > 8){
                        comboFX.UpdateComboSprite(4);
                        audioScript.PlayAudio(3);

                        if(comboCounter > 10){
                            comboFX.UpdateComboSprite(5);
                            audioScript.PlayAudio(4);
                        }
                    }
                }
            }
        }
    }

    public void ResetCombo(){
        comboCounter = 0;
    }

    IEnumerator MoveBuffer(){
        while(falling){
            coverPanel.SetActive(true);
            Debug.Log("falling");
            yield return new WaitForSeconds(1f);
        }
        yield return new WaitForSeconds(1f);
        coverPanel.SetActive(false);
        Debug.Log("not falling");
        ResetCombo();
    }

    IEnumerator ClearBuffer(){
        yield return new WaitForSeconds(0.25f);
        ApplyGravityToBoard();
        yield return new WaitForSeconds(0.25f);
    }

    IEnumerator GravityBuffer(){
        while(falling){
            yield return new WaitForSeconds(0.25f);
        }
    }

    void ApplyGravityToBoard(){
        for(int x = 0; x < w; x++){
            for(int y = (h - 1); y >= 0; y--){
                Point p = new Point(x, y);
                Block block = GetBlockAtPoint(p);
                int val = GetPointValue(p);

                // if the space is not a hole or not empty
                if(val != 0){
                    continue;
                }

                for(int ny = (y-1); ny >= -1; ny--){
                    Point next = new Point(x, ny);
                    int nextVal = GetPointValue(next);
                    if(nextVal == 0){
                        continue;
                    }
                    // did not hit an end or the edge of the board
                    if(nextVal != -1){

                        Block got = GetBlockAtPoint(next);
                        BlockPiece piece = got.GetPiece();

                        block.SetPiece(piece);
                        update.Add(piece);

                        got.SetPiece(null);
                    }

                    // hit edge
                    else{
                        // Fill in hole
                        int newVal = FillPiece();

                        BlockPiece piece;
                        Point fallPnt = new Point(x, (-3));

                        // Reuses pieces that have been matched
                        if(removed.Count > 0){
                            BlockPiece reused = removed[0];
                            reused.gameObject.SetActive(true);
                            reused.rect.anchoredPosition = GetPointPosition(fallPnt);
                            piece = reused;

                            removed.RemoveAt(0);
                        }
                        // Adds backup pieces for certain situations (e.g. starting with empty or occuppied spaces like the jelly in candy crush)
                        // should never be called in normal circumstances
                        else{
                            GameObject obj = Instantiate(nodePiece, gameBoard);
                            BlockPiece b = obj.GetComponent<BlockPiece>();
                            RectTransform rect = obj.GetComponent<RectTransform>();
                            rect.anchoredPosition = GetPointPosition(fallPnt);
                            piece = b;
                        }

                        if(removed.Count == 0){
                            falling = false;
                        }

                        piece.BlockInitialize(newVal, p, pieces[newVal - 1]);
                        Block hole = GetBlockAtPoint(p);
                        hole.SetPiece(piece);
                        ResetPiece(piece);
                        fills[x]++;
                    }
                    break;
                }
            }
        }
    }

    FlippedPieces GetFlipped(BlockPiece b){
        FlippedPieces flip = null;
        for(int i = 0; i < flipped.Count; i++){
            if(flipped[i].GetOtherPiece(b) != null){
                flip = flipped[i];
                break;
            }
        }
        return flip;
    }

    void CreateGame(){
        string seed = GetRandomSeed();
        random = new System.Random(seed.GetHashCode());
        update = new List<BlockPiece>();
        flipped = new List<FlippedPieces>();
        removed = new List<BlockPiece>();
        fills = new int[w];
        falling = false;
        coverPanel.SetActive(false);

        // Block number debugger
        SetPieceNumber(PlayerPrefs.GetInt("Blocks", 4));
        TransferSprite();

        CreateBoard();
        VerifyBoard();
        InstantiateBoard();

    }

    void TransferSprite(){
        pieces = new Sprite[PlayerPrefs.GetInt("Blocks", 5)];

        for(int t = 0; t < PlayerPrefs.GetInt("Blocks", 5); t++){
            pieces[t] = subPieces[t];
        }
    }

    void CreateBoard(){
        board = new Block[w, h];

        for(int y = 0; y < h; y++){
            for(int x =0; x < w; x++){
                board[x, y] = new Block((boardLayout.rows[y].row[x]) ? -1 : FillPiece() , new Point(x, y));
            }
        }
    }

    void VerifyBoard(){
        List<int> remove;
        for(int y = 0; y < h; y++){
            for(int x =0; x < w; x++){
                Point p = new Point(x, y);  
                int val = GetPointValue(p);
                if(val <= 0) continue;

                remove = new List<int>();
                while(isConnected(p,true).Count > 0){
                    val = GetPointValue(p);

                    if(!remove.Contains(val)){
                        remove.Add(val);
                    }
                    SetPointValue(p, NewValue(ref remove));
                }   
            }
        }
    }

    void InstantiateBoard(){
        for(int x = 0; x < w; x++){
            for(int y =0; y < h; y++){
                Block block = GetBlockAtPoint(new Point(x, y));

                int val =  block.value;
                if(val <= 0 ) continue;
                GameObject p = Instantiate(nodePiece, gameBoard);
                BlockPiece piece = p.GetComponent<BlockPiece>();
                RectTransform rect = p.GetComponent<RectTransform>();
                rect.anchoredPosition = new Vector2(32 + (64 * x), -32 - (64 * y));
                piece.BlockInitialize(val, new Point(x, y), pieces[val - 1]);

                block.SetPiece(piece);
            }
        }
    }

    int NewValue(ref List<int> remove){
        List<int> available = new List<int>();
        for(int i = 0; i < pieces.Length; i++){
            available.Add(i+1);
        }
        foreach (int i in remove){
            available.Remove(i);
        }

        if(available.Count <= 0) return 0;
        return available[random.Next(0, available.Count)];
    }

    List<Point> isConnected(Point p, bool main){
        List<Point> connected = new List<Point>();
        int val = GetPointValue(p);
        Point[] directions = 
        {
            Point.up,
            Point.right,
            Point.down,
            Point.left,
        };


        /**
        Checks for 2 or more same shapes in a direction
        oxoo or oxo
        **/
        foreach(Point dir in directions){
            List<Point> line = new List<Point>();

            int same  = 0;
            for(int i = 1; i < 3; i++){
                Point check  = Point.AddPoint(p, Point.MultiplyPoint(dir, i));
                if(GetPointValue(check) == val){
                    line.Add(check);
                    same++;
                }
            }

            if(same > 1){
                AddPoints(ref connected, line);
            }
        }

        // Checks for  ooxoo
        for(int i = 0; i < 2; i++){
            List<Point> line =  new List<Point>();

            int same = 0;
            Point[] check = {Point.AddPoint(p, directions[i]), Point.AddPoint(p, directions[i + 2])};

            foreach (Point next in check)
            {
                if(GetPointValue(next) == val){
                    line.Add(next);
                    same++;
                }
            }

            if(same > 1){
                AddPoints(ref connected, line);
            }
        }

        /**
        Checks for square blocks
        oo
        oo
        **/
        for(int i = 0; i < 4; i++){
            List<Point> square =  new List<Point>();

            int same = 0;
            int next = i + 1;
            if(next >= 4){
                next = next - 4;
            }
            Point[] check = {Point.AddPoint(p, directions[i]), Point.AddPoint(p, directions[next]), Point.AddPoint(p, Point.AddPoint(directions[i], directions[next]))};

            foreach (Point pnt in check)
            {
                if(GetPointValue(pnt) == val){
                    square.Add(pnt);
                    same++;
                }
            }

            if(same > 2){
                AddPoints(ref connected, square);
            }
        }

        if(main){
            for(int i = 0; i < connected.Count; i++){
                AddPoints(ref connected, isConnected(connected[i], false));
            }
        }

        return connected;
    }

    void AddPoints(ref List<Point> points, List<Point> add){
        foreach (Point p in add)
        {
            bool doAdd = true;
            for(int i = 0; i < points.Count; i++){
                if(points[i].PointEquals(p)){
                    doAdd = false;
                    break;
                }
            }

            if(doAdd) points.Add(p);
        }
    }

    int GetPointValue(Point p){
        if(p.x < 0 || p.x >= w || p.y < 0 || p.y >= h) return -1;
        return board[p.x, p.y].value;
    }

    public Vector2 GetPointPosition(Point p){
        return new Vector2(32 + (64 * p.x), -32 - (64 * p.y));
    }

    void SetPointValue(Point p, int v){
        board[p.x, p.y].value = v;
    }

    public void ResetPiece(BlockPiece piece){
        piece.ResetPosition();
        update.Add(piece);
    }

    public void FlipPieces(Point p1, Point p2, bool main){
        if(GetPointValue(p1) < 0){
            return;
        }

        Block block1 = GetBlockAtPoint(p1);
        BlockPiece piece1 = block1.GetPiece();

        if(GetPointValue(p2) > 0){
            Block block2 = GetBlockAtPoint(p2);
            BlockPiece piece2 = block2.GetPiece();

            block1.SetPiece(piece2);
            block2.SetPiece(piece1);

            if(main){
                flipped.Add(new FlippedPieces(piece1, piece2));
                // Add a monster counter
                if(scene.name != "TutorialAttack"){
                    combat.EnemyAttack();
                }
            }

            update.Add(piece1);
            update.Add(piece2);

        }
        else{
            ResetPiece(piece1);
        }
    }

    Block GetBlockAtPoint(Point p){
        return board[p.x, p.y];
    }

    int FillPiece(){
        int val = 1;
        val = (random.Next(0, 100) / (100 / pieces.Length)) + 1;
        return val;
    }

    string GetRandomSeed(){
        string seed = "";
        string characters = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm0123456789!@#$%^&*()";

        for(int i = 0; i < 20; i++){
            seed += characters[Random.Range(0, characters.Length)];
        }
        return seed;
    }
}

[System.Serializable]
public class Block{
    public int value;
    public Point index;
    BlockPiece piece;

    public Block(int v, Point i){
        value = v;
        index = i;
    }

    public void SetPiece(BlockPiece p){

        piece = p;
        value = (piece == null) ? 0 : piece.value;

        if(piece == null){
            return;
        }

        piece.SetIndex(index);
    }

    public BlockPiece GetPiece(){
        return piece;
    }
}

[System.Serializable]
public class FlippedPieces{
    public BlockPiece b1;
    public BlockPiece b2;

    public FlippedPieces(BlockPiece f, BlockPiece g){
        b1 = f;
        b2 = g;
    }

    public BlockPiece GetOtherPiece(BlockPiece b){
        if(b == b1){
            return b2;
        }
        else if(b == b2){
            return b1;
        }
        else{
            return null;
        }
    }
}