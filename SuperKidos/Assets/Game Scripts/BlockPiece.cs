﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BlockPiece : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    public int value;
    public int fxValue;
    public Point index;

    [HideInInspector]
    public Vector2 pos;

    [HideInInspector]
    public RectTransform rect;

    Image image;
    Point clearedBlock;
    bool updating;

    private Animator bubbleAnimator;


    public void BlockInitialize(int v, Point p, Sprite piece){
        image = GetComponent<Image>();
        rect = GetComponent<RectTransform>();
        bubbleAnimator = GetComponent<Animator>();

        value = v;
        SetIndex(p);
        SwitchAnim(value);
        image.sprite = piece;
    }

    public Point GetClearedBlock(){
        return clearedBlock;
    }

    public void SetClearedBlock(Point p){
        clearedBlock = p;
    }

    public void EffectsInitialize(int v, Point p, Sprite piece){
        image = GetComponent<Image>();
        rect = GetComponent<RectTransform>();
        bubbleAnimator = GetComponent<Animator>();

        value = v;
        SetIndex(p);
        SwitchFX(value);
        image.sprite = piece;
    }

    public void PlayPop(){
        switch(fxValue){
            case 1 :
                bubbleAnimator.Play("Base.White_Pop", 0, 0f);
                break;
            case 2 :
                bubbleAnimator.Play("Base.White_Pop", 0, 0f);
                break;
            case 3 :
                bubbleAnimator.Play("Base.White_Pop", 0, 0f);
                break;
            case 4 :
                bubbleAnimator.Play("Base.White_Pop", 0, 0f);
                break;
            case 5 :
                bubbleAnimator.Play("Base.White_Pop", 0, 0f);
                break;
            default : 
                break;
        }
    }

    public void SwitchAnim(int value){
        switch(value){
            case 1 :
                bubbleAnimator.Play("Base.Red_Idle", 0, 0f);
                break;
            case 2 :
                bubbleAnimator.Play("Base.Green_Idle", 0, 0f);
                break;
            case 3 :
                bubbleAnimator.Play("Base.Blue_Idle", 0, 0f);
                break;
            case 4 :
                bubbleAnimator.Play("Base.Black_Idle", 0, 0f);
                break;
            case 5 :
                bubbleAnimator.Play("Base.Yellow_Idle", 0, 0f);
                break;
            default : 
                break;
        }
    }

    public void SwitchFX(int value){
        switch(value){
            case 1 :
                fxValue = value;
                break;
            case 2 :
                fxValue = value;
                break;
            case 3 :
                fxValue = value;
                break;
            case 4 :
                fxValue = value;
                break;
            case 5 :
                fxValue = value;
                break;
            default : 
                break;
        }
    }

    public int GetBlockValue(){
        return value;
    }

    public void SetIndex(Point p){
        index = p;
        ResetPosition();
        UpdateName();
    }

    public Point GetIndex(){
        return index;
    }

    public void ResetPosition(){
        pos = new Vector2(32 + (64 * index.x), -32 - (64 * index.y));
    }

    public void MovePosition(Vector2 move){
        rect.anchoredPosition += move * Time.deltaTime * 9.81f;
    }

    public void MovePositionTo(Vector2 move){
        rect.anchoredPosition = Vector2.Lerp(rect.anchoredPosition, move, Time.deltaTime * 9.81f);
    }

    void UpdateName(){
        transform.name = "Block [" + index.x + ", " + index.y + "]";
    }

    public bool UpdatePiece(){
        if(Vector3.Distance(rect.anchoredPosition, pos) > 1){
            MovePositionTo(pos);
            updating = true;
            return true;
        }
        else{
            rect.anchoredPosition = pos;
            updating = false;
            return false;
        }
    }

    public void OnPointerUp(PointerEventData eventData){
        MoveBlock.instance.DropPiece();
    }

    public void OnPointerDown(PointerEventData eventData){
        if(updating){
            return;
        }
        MoveBlock.instance.MovePiece(this);
    }
}
