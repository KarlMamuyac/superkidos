﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialScript : MonoBehaviour
{
    public CombatScript combat;

    bool defending;
    bool attacking;
    bool healing;
    void Start()
    {
        combat.EnemyAttackTutorial();
        Debug.Log("running");
        healing = true;
        defending = false;
        attacking = false;
    }

    public void TutorialCombat(int value){
        switch(value){
            case 1 :
                if(attacking){
                    combat.Combat(value);
                    attacking = false;
                }
                break;
            case 2 :
                if(healing){
                    combat.Combat(value);
                    healing = false;
                    defending = true;
                }
                break;
            case 3 :
                if(defending){
                    combat.Combat(value);
                    MonsterAttack();
                }
                break;
            case 4 :
                break;
            default : 
                break;
        }
    }

    void MonsterAttack(){
        defending = false;
        attacking = true;
        combat.EnemyAttackTutorial();
    }
}
