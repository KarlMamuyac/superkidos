﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyScript : MonoBehaviour
{
    public Text energyText;
    private int currentEnergy;

    void Start()
    {
        currentEnergy = PlayerPrefs.GetInt("Energy", 5);
    }

    void Update()
    {
        energyText.text = (currentEnergy).ToString();
    }
}
