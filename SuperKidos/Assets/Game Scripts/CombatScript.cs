﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;

public class CombatScript : MonoBehaviour
{
    public GameObject enemy;
    public PlayerScript player;
<<<<<<< HEAD
    public EnemyScript enemy;    

    public Animator girlMove;

    public Animator healAnim;
    public Animator shieldAnim;
    public Animator screenAnim;

    public SkeletonGraphic girlSke;
    public SkeletonGraphic boySke;
    public SkeletonGraphic dogSke;
    public SkeletonGraphic monsterSke;

    private Skeleton girlSkin;
    private Skeleton boySkin;
    private Skeleton dogSkin;
    private Skeleton monsterSkin;

    bool shielded;

    void Start()
    {
        girlSkin = girlSke.Skeleton;
        boySkin = boySke.Skeleton;
        dogSkin = dogSke.Skeleton;
        monsterSkin = monsterSke.Skeleton;

        // girlSkin.SetSkin("Girl_Default");
    }

    public void EnemyAttack(){
        enemy.AddTurn(1);
        if(enemy.TurnCounter() ==  3){
            PlayPlayerTakeDamage();
        }
=======
    public EnemyScript enemyScript;   

    public EnemyAnimationHandler enemyAnim;

    public PlayerAnimationHandler partyAnim;
    public void SwitchEnemies(){
        enemy.SetActive(true);
    }

    public void EnemyAttack(){
        if(enemy.activeSelf){
            enemyScriptAttack();
        }
    }

    public void enemyScriptAttack(){
        enemyScript.AddTurn(1);
        if(enemyScript.TurnCounter() ==  3){
            enemyAnim.PlayAttack();

            if(player.GetTotalBlock() > 0){
                partyAnim.PlayBreak();
            }
            player.LoseHealth(enemyScript.DealHeavyDamage());

        }
    }

    public void EnemyAttackTutorial(){
        enemyAnim.PlayAttack();
        player.LoseHealth(enemyScript.DealHeavyDamage());
>>>>>>> development
    }

    public void Combat(int value){
        switch(value){
            case 1 :
<<<<<<< HEAD
                PlayPlayerHit();
                break;
            case 2 :
                PlayPlayerHeal();
                break;
            case 3 :
                PlayPlayerBlock();
=======
                StartCoroutine(PartyAttack());
                break;
            case 2 :
                StartCoroutine(PartyHeal());
                break;
            case 3 :
                PartyDefend();
>>>>>>> development
                break;
            case 4 :
                break;
            default : 
                break;
        }
    }

<<<<<<< HEAD
    void PlayPlayerHit(){
        enemy.LoseHealth(player.DealDamage());
        StartCoroutine(HitAnim());
    }

    void PlayPlayerTakeDamage(){
        player.LoseHealth(enemy.DealHeavyDamage());
        StartCoroutine(HurtAnim());
        shieldAnim.Play("Base.burst", 0, 0f);
        shielded = false;
    }

    void PlayPlayerHeal(){
        player.HealPlayer();
        StartCoroutine(HealAnim());
        healAnim.Play("Base.heal", 0, 0f);
    }

    void PlayPlayerBlock(){
        player.AddBlock();
        StartCoroutine(BlockAnim());
        if(!shielded){
            shieldAnim.Play("Base.build", 0, 0f);
            shielded = true;
        }
    }

    IEnumerator HealAnim(){
        yield return new WaitForSpineAnimationComplete(dogSke.AnimationState.SetAnimation(0, "Heal", false));
        PlayDogDefault();
    }

    IEnumerator HitAnim(){
        yield return new WaitForSpineAnimationComplete(girlSke.AnimationState.SetAnimation(0, "Pre-Attack", false));
        girlMove.Play("Base.fly_out", 0, 0f);
        yield return new WaitForSpineAnimationComplete(girlSke.AnimationState.SetAnimation(0, "Charged-Attack", false));
        PlayGirlDefault();
        yield return new WaitForSpineAnimationComplete(monsterSke.AnimationState.SetAnimation(0, "Hit", false));
        PlayMonsterDefault();
    }

    IEnumerator BlockAnim(){
        yield return new WaitForSpineAnimationComplete(boySke.AnimationState.SetAnimation(0, "Defend_Dragon", false));
        PlayBoyDefault();
    }

    IEnumerator HurtAnim(){
        yield return new WaitForSpineAnimationComplete(monsterSke.AnimationState.SetAnimation(0, "Attack", false));
        screenAnim.Play("Base.shake", 0, 0f);
        PlayMonsterDefault();
        dogSke.AnimationState.SetAnimation(0, "Hit", false);
        boySke.AnimationState.SetAnimation(0, "Hit_Dragon", false);
        yield return new WaitForSpineAnimationComplete(girlSke.AnimationState.SetAnimation(0, "Hit", false));
        PlayGirlDefault();
        PlayBoyDefault();
        PlayDogDefault();
    }

    void PlayGirlDefault(){
        girlSke.AnimationState.SetAnimation(0, "Idle", true);
    }

    void PlayBoyDefault(){
        boySke.AnimationState.SetAnimation(0, "Idle_Dragon", true);
    }

    void PlayDogDefault(){
        dogSke.AnimationState.SetAnimation(0, "Idle", true);
    }

    void PlayMonsterDefault(){
        monsterSke.AnimationState.SetAnimation(0, "Idle", true);
    }
=======
    IEnumerator PartyAttack(){
        partyAnim.PlayAttack();
        yield return new WaitForSeconds(0.75f);
        EnemyLoseHealth();
    }

    IEnumerator PartyHeal(){
        partyAnim.PlayHeal();
        yield return new WaitForSeconds(0.5f);
        player.HealPlayer();
    }

    void PartyDefend(){
        player.AddBlock();
        partyAnim.PlayDefend();
    }

    public void EnemyLoseHealth(){
        if(enemy.activeSelf){
            enemyScript.LoseHealth(player.DealDamage());
            enemyAnim.PlayHit();
        }
    }

>>>>>>> development
}
