﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelHandler : MonoBehaviour
{
<<<<<<< HEAD
    private int currentEnergy;

    void Start()
    {
        currentEnergy = PlayerPrefs.GetInt("Energy", 5);
    }

    public void ReduceEnergy(){
        currentEnergy--;
        PlayerPrefs.SetInt("Energy", currentEnergy);
    }

    public void RedirectToShop(){
        if(currentEnergy <= 0){
            LoadShop();
        }
    }

    public void LoadNextLevel(string levelName){
        if(currentEnergy <= 0){
            LoadShop();
        }
        else{
            ReduceEnergy();
            SceneManager.LoadScene(levelName);
        }
    }

    public void ReloadLevel(){
        if(currentEnergy <= 0){
            LoadShop();
        }
        else{
            ReduceEnergy();
            Scene scene = SceneManager.GetActiveScene(); 
            SceneManager.LoadScene(scene.name);
        }
    }

    public void LoadMenu(){
        SceneManager.LoadScene("MainMenu");
=======
    public Animator loaderAnim;
    float startTime;

    void Start()
    {
        loaderAnim.Play("Base.start", 0, 0f);
        startTime = loaderAnim.GetCurrentAnimatorStateInfo(0).length;
    }

    public void LoadLevel(string levelName){
        loaderAnim.Play("Base.end", 0, 0f);
        StartCoroutine(AnimationToLoad(loaderAnim.GetCurrentAnimatorStateInfo(0).length, levelName));
    }

    public void ReloadLevel(){
        loaderAnim.Play("Base.end", 0, 0f);
        Scene scene = SceneManager.GetActiveScene(); 
        StartCoroutine(AnimationToLoad(loaderAnim.GetCurrentAnimatorStateInfo(0).length, scene.name));
    }

    public void LoadMenu(){
        loaderAnim.Play("Base.end", 0, 0f);
        StartCoroutine(AnimationToLoad(loaderAnim.GetCurrentAnimatorStateInfo(0).length, "StartMenu"));
>>>>>>> development
    }

    public void LoadShop(){
        SceneManager.LoadScene("ShopMenu");
    }

    public void LoadLevelSelect(){
        SceneManager.LoadScene("LevelSelect");
    }

    public float GetStartTime(){
        return startTime;
    }

    IEnumerator AnimationToLoad(float delay, string levelName){
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(levelName);
    }
}
