﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
Properties of a point inside the game board.
Location and transformations included.
**/

[System.Serializable]
public class Point {
    public int x;
    public int y;

    public Point(int newx, int newy){
        x = newx;
        y = newy;
    }

    public void MultiplyPoint(int m){
        x = x * m;
        y = y * m;
    }

    public void DividePoint(int d){
        x = x/d;
        y = y/d;
    }

    public void AddPoint(Point p){
        x = x + p.x;
        y = y + p.y;
    }

    public void SubPoint(Point p){
        x = x - p.x;
        y = y - p.y;
    }

    public Vector2 PointToVector(){
        return new Vector2(x, y);
    }

    public bool PointEquals(Point p){
        return (x == p.x && y == p.y);
    }

    public static Point PointFromVector(Vector2 v){
        return new Point((int)v.x, (int)v.y);
    }

    public static Point PointFromVector(Vector3 v){
        return new Point((int)v.x, (int)v.y);
    }

    public static Point MultiplyPoint(Point p, int m){
        return new Point(p.x * m, p.y * m);
    }

    public static Point DividePoint(Point p, int d){
        return new Point(p.x / d, p.y / d);
    }

    public static Point AddPoint(Point p, Point p2){
        return new Point(p.x + p2.x, p.y + p2.y);
    }

    public static Point SubPoint(Point p, Point p2){
        return new Point(p.x - p2.x, p.y - p2.y);
    }

    public static Point ClonePoint(Point p){
        return new Point(p.x, p.y);
    }

    public static Point zero{
        get{ return new Point(0, 0); }
    }
    public static Point one{
        get{ return new Point(1, 1); }
    }
    public static Point up{
        get{ return new Point(0, 1); }
    }
    public static Point down{
        get{ return new Point(0, -1); }
    }
    public static Point left{
        get{ return new Point(-1, 0); }
    }
    public static Point right{
        get{ return new Point(1, 0); }
    }
}
